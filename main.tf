terraform {
  required_providers {
    aws = {
      source  = "hashicorp/aws"
      version = "5.17.0"
    }
  }
}

provider "aws" {
  region = "eu-west-1"
}

resource "aws_vpc" "main_vpc" {
  cidr_block = "10.0.0.0/16"
  tags = {
    Name = "${var.environment}-${var.app_prefix}-vpc"
  }
}


resource "aws_internet_gateway" "gw" {
  vpc_id = aws_vpc.main_vpc.id

  tags = {
    Name = "${var.environment}-${var.app_prefix}-igw"
  }
}

resource "aws_eip" "eip_nat_gw" {
  count = length(var.availability_zones)

  domain     = "vpc"
  depends_on = [aws_internet_gateway.gw]

}

resource "aws_nat_gateway" "nat_gw" {
  count = length(var.availability_zones)

  subnet_id         = element(aws_subnet.public_subnet[*].id, count.index)
  connectivity_type = "public"
  allocation_id     = element(aws_eip.eip_nat_gw[*].id, count.index)
  tags = {
    Name = "${var.environment}-${var.app_prefix}-nat-gw-${count.index}"
  }
  depends_on = [aws_internet_gateway.gw]
}

resource "aws_subnet" "public_subnet" {
  count = length(var.availability_zones)

  vpc_id            = aws_vpc.main_vpc.id
  cidr_block        = var.public_subnet_cidr_blocks[count.index]
  availability_zone = var.availability_zones[count.index]
  tags = {
    Name = "${var.environment}-${var.app_prefix}-public-subnet-${count.index}"
  }
}

resource "aws_subnet" "private_subnet" {
  count = length(var.availability_zones)

  vpc_id            = aws_vpc.main_vpc.id
  cidr_block        = var.private_subnet_cidr_blocks[count.index]
  availability_zone = var.availability_zones[count.index]
  tags = {
    Name = "${var.environment}-${var.app_prefix}-private-subnet-${count.index}"
  }
}

resource "aws_route_table" "private_route_table" {
  count = length(var.availability_zones)

  vpc_id = aws_vpc.main_vpc.id

  route {
    cidr_block = "0.0.0.0/0"
    gateway_id = element(aws_nat_gateway.nat_gw[*].id, count.index)
  }

  tags = {
    Name = "${var.environment}-${var.app_prefix}-private-route-table-${count.index}"
  }
}

resource "aws_route_table" "public_route_table" {
  vpc_id = aws_vpc.main_vpc.id

  route {
    cidr_block = "0.0.0.0/0"
    gateway_id = aws_internet_gateway.gw.id
  }

  tags = {
    Name = "${var.environment}-${var.app_prefix}-public-route-table"
  }
}

resource "aws_route_table_association" "public_route_table_to_subnet" {
  count = length(var.availability_zones)

  subnet_id      = element(aws_subnet.public_subnet[*].id, count.index)
  route_table_id = aws_route_table.public_route_table.id
}

resource "aws_route_table_association" "private_route_table_to_subnet" {
  count = length(var.availability_zones)

  subnet_id      = element(aws_subnet.private_subnet[*].id, count.index)
  route_table_id = element(aws_route_table.private_route_table[*].id, count.index)
}

data "aws_ami" "amzn-linux-2023-ami" {
  most_recent = true
  owners      = ["amazon"]

  filter {
    name   = "name"
    values = ["al2023-ami-2023.*-x86_64"]
  }
}

resource "aws_key_pair" "web_key" {
  key_name   = "web-key"
  public_key = "ssh-rsa AAAAB3NzaC1yc2EAAAADAQABAAABgQCzZboccf+subMoJc/fgrQh62IbHaLfrD1IScYJPpsj/RCSFJwUL9KqUhLpMtqQvzNjrJx8T9O9p3OOgbB8n7LB1skrH8e3xIeGQzPm07KcnKVtxcy9L/WOQUt1asOQz+jXQPGaanl0td04KD1/9ytkk7UXekG2kO7/IfvlRQqnxC5mINpn3cjt/0RRN6K9MQpnt61SoWRUWTX3X4j5xLNm6+2PhRWs8P8p6uUIfTUZCVdX3N4ypwO+FctMxw11xwTD56BTcg4y9Bt12BQrelTBm+Xccypp/EWOzcCGDmCiEAHvPXAhAncaWTpGKKZ1CLbZUH1lILv0q4el9/jBEfuj0K6DtQqzztROLLeub/LY8jm1U+Vk7YZS5cgh95npKHVjn951Gq/YeULaP3E0xAhc3JCNoaJLZ5EPD0yMsGVEeGGhHX/a45Be+dabTCqkLQj11zPOhYXUG6pA8dsFJ1ftiZW75IeVHqcsojIb+tpiTt9RKjVkGxpxEBo/q+oNBaE= doriants@fedora"
}

resource "aws_security_group" "web_security_group" {
  name        = "web-security-group"
  description = "Security group for the web application"
  vpc_id      = aws_vpc.main_vpc.id

  ingress {
    from_port   = 8080
    to_port     = 8080
    protocol    = "tcp"
    cidr_blocks = ["${aws_vpc.main_vpc.cidr_block}"]
  }

  egress {
    from_port   = 0
    to_port     = 0
    protocol    = "-1"
    cidr_blocks = ["0.0.0.0/0"]
  }
}




resource "aws_instance" "web" {
  count = length(var.availability_zones)

  ami               = data.aws_ami.amzn-linux-2023-ami.id
  instance_type     = "t2.micro"
  subnet_id         = element(aws_subnet.private_subnet[*].id, count.index)
  key_name          = aws_key_pair.web_key.key_name
  security_groups   = [aws_security_group.web_security_group.id]
  availability_zone = var.availability_zones[count.index]
  user_data         = <<-EOF
              #!/bin/bash
              sudo yum install -y docker
              sudo systemctl enable --now docker
              EOF

  tags = {
    Name = "${var.environment}-${var.app_prefix}-web-instance-${count.index}"
  }
}

#=============================================================================
## Declaring Load Balancer for the application

resource "aws_lb_target_group" "web_target_group" {
  name     = "${var.environment}-web-target-group"
  port     = 8080
  protocol = "HTTP"
  vpc_id   = aws_vpc.main_vpc.id

  tags = {
    Name = "${var.environment}-${var.app_prefix}-web-target-group"
  }
}

resource "aws_lb_target_group_attachment" "targets" {
  count = length(aws_instance.web)

  target_group_arn = aws_lb_target_group.web_target_group.arn
  target_id        = element(aws_instance.web[*].id, count.index)
  port             = 8080
}

data "aws_security_group" "sg_default" {
  name = "default" 
}

resource "aws_lb" "web_lb" {
  name               = "${var.environment}-web-lb"
  internal           = false
  load_balancer_type = "application"
  security_groups    = [data.aws_security_group.sg_default.id]
  subnets            = [for subnet in aws_subnet.public_subnet : subnet.id]

  enable_deletion_protection = false

  tags = {
    Name = "${var.environment}-${var.app_prefix}-web-lb"
  }
}

resource "aws_lb_listener" "front_end" {
  load_balancer_arn = aws_lb.web_lb.arn
  port              = "80"
  protocol          = "HTTP"
  
    default_action {
    type             = "forward"
    target_group_arn = aws_lb_target_group.web_target_group.arn
  }
}



#=============================================================================
